/**
 * 일반 이미지 이름에  XXXX_mouseover.png 를 붙인다.
 * @param value 이미지 이름
 */
function getMouseoverImageName(imageSrc) {
    var prefix = imageSrc.substring(0, imageSrc.lastIndexOf("/")+1);
    imageSrc = imageSrc.substr(imageSrc.lastIndexOf("/")+1);
    if(imageSrc.indexOf("mouseover.png") == -1) {
        imageSrc = imageSrc.replace(".png", "_mouseover.png");
    }
//            console.log(prefix + imageSrc);
    return prefix + imageSrc;
}

/**
 * XXX_mouseover.png 를 XXX.png 로 변경한다.
 * @param value mouseover 이미지 이름
 */
function getOriginalImageName(imageSrc) {
    var prefix = imageSrc.substring(0, imageSrc.lastIndexOf("/")+1);
    imageSrc = imageSrc.substr(imageSrc.lastIndexOf("/")+1);
    imageSrc = imageSrc.replace("_mouseover", '');
//            console.log(prefix+imageSrc);
    return prefix + imageSrc;
}

/**
 * XXX_mouseover.png 를 XXX.png 로 변경한다.
 * @param value mouseover 이미지 이름
 */
function removeToggleNMouseover(imageSrc) {
    var prefix = imageSrc.substring(0, imageSrc.lastIndexOf("/")+1);
    imageSrc = imageSrc.substr(imageSrc.lastIndexOf("/")+1);
    imageSrc = imageSrc.replace("_mouseover", '');
    imageSrc = imageSrc.replace("_toggle", '');
    imageSrc = imageSrc.replace("_off", '');
    return prefix + imageSrc;
}

/**
 * 토글 이미지 정보를 조회한다.
 * @param value mouseover 이미지 이름
 */
function getToggleImageName(imageSrc) {
    var prefix = imageSrc.substring(0, imageSrc.lastIndexOf("/")+1);
    imageSrc = imageSrc.substr(imageSrc.lastIndexOf("/")+1);
    if(imageSrc.indexOf("_toggle") == -1) {
        imageSrc = imageSrc.replace(".png", "_toggle.png");
    } else {
        imageSrc = imageSrc.replace("_toggle.png", ".png");
    }
    return prefix + imageSrc;
}

function getToggleOnImageName(imageSrc) {
    var prefix = imageSrc.substring(0, imageSrc.lastIndexOf("/")+1);
    imageSrc = imageSrc.substr(imageSrc.lastIndexOf("/")+1);
    imageSrc = imageSrc.replace("_toggle.png", ".png");
    return prefix + imageSrc;
}

function getToggleOffImageName(imageSrc) {
    var prefix = imageSrc.substring(0, imageSrc.lastIndexOf("/")+1);
    imageSrc = imageSrc.substr(imageSrc.lastIndexOf("/")+1);
    if(imageSrc.indexOf('_toggle') == -1) {
        imageSrc = imageSrc.replace(".png", "_toggle.png");
    }

    return prefix + imageSrc;
}

function getOffImageName(imageSrc) {
    var prefix = imageSrc.substring(0, imageSrc.lastIndexOf("/")+1);
    imageSrc = imageSrc.substr(imageSrc.lastIndexOf("/")+1);

    if(imageSrc.indexOf("_toggle") == -1) {
        if(imageSrc.indexOf("_off") == -1) {
            imageSrc = imageSrc.replace(".png", "_off.png");
        }else {
            imageSrc = imageSrc.replace("_off.png", ".png");
        }
    }else {
        imageSrc = imageSrc.replace("_toggle.png", "_off.png");
    }
    return prefix + imageSrc;
}

function convertImgToBase64(url, callback, outputFormat){
    var canvas = document.createElement('CANVAS'),
        ctx = canvas.getContext('2d'),
        img = new Image;
    img.crossOrigin = 'Anonymous';
    img.onload = function(){
        var dataURL;
        canvas.height = img.height;
        canvas.width = img.width;
        ctx.drawImage(img, 0, 0);
        dataURL = canvas.toDataURL(outputFormat);
        callback.call(this, dataURL);
        canvas = null;
    };
    img.src = url;
}

function extractedAdjustScale() {
    if(screen.deviceXDPI) {

    }
    var zoomLevel = 0.8;
    $("body").css("zoom", zoomLevel);
    $(".graphImage").css("zoom", zoomLevel);
    var obj = $(".graphImage").offset();
    var left = obj.left * zoomLevel;
    var top = obj.top * zoomLevel;
    $(".graphImage").css("left", left + "px");
    $(".graphImage").css("top", top + "px");
}

function adjustScaleForXDPIdevice() {

    var constantZoom = 96;
    var zoomLevel;

    console.log(screen.deviceXDPI +" // "+ screen.logicalXDPI +" // "+ screen.systemXDPI);

    zoomLevel = constantZoom / screen.systemXDPI;

//    if((screen.deviceXDPI == screen.logicalXDPI) && (screen.deviceXDPI > constantZoom)) {
//        zoomLevel = constantZoom / screen.logicalXDPI;
//    }
    console.log(zoomLevel);


    if (screen.deviceXDPI) {

/*        $("body").css("zoom", zoomLevel);
        $(".graphImage").css("zoom", zoomLevel);
        var obj = $(".graphImage").offset();
        var left = obj.left * zoomLevel;
        var top = obj.top * zoomLevel;
        $(".graphImage").css("left", left + "px");
        $(".graphImage").css("top", top + "px");*/

        if (screen.deviceXDPI > 96) {
            $("body").css("zoom", 0.7);
            $(".graphImage").css("zoom", 0.7);
            var obj = $(".graphImage").offset();
            var left = obj.left * 0.7;
            var top = obj.top * 0.7;
            $(".graphImage").css("left", left + "px");
            $(".graphImage").css("top", top + "px");
        }else {
            //extractedAdjustScale();
        }
    }
}

function adjustViewerScaleForXDPIdevice() {

    var constantZoom = 96;
    var zoomLevel;

    console.log(screen.deviceXDPI +" // "+ screen.logicalXDPI +" // "+ screen.systemXDPI);

    zoomLevel = constantZoom / screen.systemXDPI;

//    if((screen.deviceXDPI == screen.logicalXDPI) && (screen.deviceXDPI > constantZoom)) {
//        zoomLevel = constantZoom / screen.logicalXDPI;
//    }
    console.log(zoomLevel);


    /*if (screen.deviceXDPI) {

        $("body").css("zoom", zoomLevel);
        $(".graphImage").css("zoom", zoomLevel);
        var obj = $(".graphImage").offset();
        var left = obj.left * zoomLevel;
        var top = obj.top * zoomLevel;
        $(".graphImage").css("left", left + "px");
        $(".graphImage").css("top", top + "px");


        $(".graphImage").css("left", left + "px");
        $(".graphImage").css("top", top + "px");
        $(".cutImages").css("left", left + "px");
        $(".cutImages").css("top", top + "px");
    }*/

    if (screen.deviceXDPI) {
        if (screen.deviceXDPI > 96) {
            zoomLevel = 0.9;
            $("body").css("zoom", zoomLevel);
            $(".graphImage").css("zoom", zoomLevel);
            $('.cutImages').css("zoom", zoomLevel);
            var obj = $(".graphImage").offset();
            var left = obj.left * zoomLevel;
            var top = obj.top * zoomLevel;
            $(".graphImage").css("left", left + "px");
            $(".graphImage").css("top", top + "px");
            $(".cutImages").css("left", left + "px");
            $(".cutImages").css("top", top + "px");
        }
    }
}

/**
 * [ghseong 2015.03.16] ios 용 script head tag에 추가
 */
function addiPadJS(iPadArr){
    var javascript;

    for(var i = 0; i< iPadArr.length; i++){
        javascript = $('<script>',
            {
                type : 'text/javascript',
                src  : iPadArr[i]
            });
        $('head').append(javascript);
    }
}


