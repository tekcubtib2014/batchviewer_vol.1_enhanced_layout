var contentsArray =

{
    "enabledContents": [

        {
            "year": 6,
            "categ": 1,
            "step": 1,
            "tit_thumb": "大昔のくらし　縄文時代",
            "tit": "縄文時代のくらし",
            "cd": "04-MT000101-1006001"
        },
        {
            "year": 6,
            "categ": 3,
            "step": 12,
            "tit_thumb": "徳川家光と大名行列",
            "tit": "大名の配置",
            "cd": "04-MT000101-1006012"
        },
    ]
};