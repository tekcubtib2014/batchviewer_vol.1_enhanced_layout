
/**
 * 탭을 구성할 배열
 * */
var CONTENTS_ARRAY = [ [], [], [], [], [], [] ];


var is_iPad = navigator.userAgent.match(/iPad/i) != null;

/**
 * JS 파일을 참조하여 배열을 완성한다.
 * */
function makeArray() {
    for(var j = 1; j < CONTENTS_ARRAY.length + 1; j++) {
        CONTENTS_ARRAY[j - 1] = [];
    }

    for(var i = 0, len = contentsArray.enabledContents.length; i < len ; i++) {
        var contentsInfo = contentsArray.enabledContents[i];
        var categ = contentsInfo.categ;
        for(var j = 1; j < CONTENTS_ARRAY.length + 1; j++) {
            if (categ == j) {
                CONTENTS_ARRAY[categ - 1].push(contentsInfo);
            }
        }
    }
}

/**
 * 도움말 페이지 데이터
 * @type {{id: string, images: string[]}[]}
 */
var helpPages = [
      {id : 'help01', images: ["002.png","003.png","004.png","005.png","006.png"]}
    , {id : 'help02', images: ["007.png","008.png","009.png","010.png","011.png"]}
    , {id : 'help03', images: ["012.png","013.png","014.png","015.png","016.png", "017.png", "018.png"]}
    , {id : 'help04', images: ["019.png","020.png","021.png"]}
];

var tabId;
var helpImgsPath = "img/help/";
var currentHelpPage = [];
var count = 1;
var helpFlag = true;
var isChrome = window.chrome;

function initLogger() {
    var appender = new HttpPostAppender('appender');
    var logger = Logger.getInstance({mainAppender: appender});

    //
    // 라이선스 파일이 있는 경우 모든 컨텐츠 표시
    //
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
        fileSystem.root.getFile(Enum.Const.LICENSE_FLAG, {create: false, exclusive: false}, licenseSuccess, licenseFail);
    });

    //
    // Statusbar overlay config
    //
    StatusBar.overlaysWebView(false);
}

function licenseSuccess() {
    contentsArray.enabledContents = fullContentsArray.enabledContents;
//    $("#certConfigBtn").hide();
    makeArray();
//    fillContents("#section-history");
    initTabContents();
}

function licenseFail() {
    $(".certBtn").show();
}

/**
 * 썸네일 이미지로 탭을 구성한다.
 * @param index
 */
function fillContents(index) {
    var tabContentsArray;
    switch (index) {
        case "#section-history" :
            tabContentsArray = CONTENTS_ARRAY[0];
            break;
        case "#section-livestock" :
            tabContentsArray = CONTENTS_ARRAY[1];
            break;
        case "#section-fishery" :
            tabContentsArray = CONTENTS_ARRAY[2];
            break;
        case "#section-industry" :
            tabContentsArray = CONTENTS_ARRAY[3];
            break;
        case "#section-trade" :
            tabContentsArray = CONTENTS_ARRAY[4];
            break;
        case "#section-domestic" :
            tabContentsArray = CONTENTS_ARRAY[5];
            break;
    }
    $(index).empty();

    var row = $("<div>").addClass("row");

    $(index).append(row);
    for(var i = 0, len = tabContentsArray.length; i < len;i++) {
        var url = tabContentsArray[i].cd + ".html";
        var imagePath = "img/thumbnail/" + tabContentsArray[i].cd + ".png";

        var column = $("<div>").addClass("column");
        var anchor = $("<a>").addClass("anchor");

        $("<img>").css("border", "none");
        $("<img>").attr("src", imagePath).appendTo(anchor);
        $(anchor).appendTo(column);
        $(column).appendTo(row);

        $(index + ' div' + ' a:last').attr('href', "module/" +url);
    }
    $(".row").each(function() {
        var row = this;
        if($(row).children().size() == 0 ) {
            $(row).append("<img class='no_content'>");
        }
    });
    $(".no_content").attr("src", "img/thumbnail/no_content.png");

}

/**
 *
 */
function initTabContents() {
    $('.tabButtons').each(function() {
//        console.log("this.href = " + this.href.substring(this.href.lastIndexOf('#')));
        fillContents(this.href.substring(this.href.lastIndexOf('#')));

        var anchor = $(this);
        $(this).bind('click', function() {
            tabId = this.href.substring(this.href.lastIndexOf('#'));
//            console.log("this.href.substring(this.href.lastIndexOf('#')) = " + this.href.substring(this.href.lastIndexOf('#')));
            $(".content-current").each(function() {
                $(this).removeClass('content-current');
            });
            $(tabId).addClass("content-current");
            $('.content section').css('border', '9px #' + anchor[0].id + ' solid');
            $('#btnBack').show();
            var currentTabUrl = tabId.replace("#", "?tab=");

            if(isChrome) {
                location.href = "index.html" + currentTabUrl;
            }
        });
    });
}

/**
 * 도움말 페이지 레이아웃 설정
 * @param el
 */
function layer_open(el){
    var temp = $('#' + el);
    var bg = temp.prev().hasClass('bg');	//dimmed 레이어를 감지하기 위한 boolean 변수

    if(bg){
        $('.layer').fadeIn();	//'bg' 클래스가 존재하면 레이어가 나타나고 배경은 dimmed 된다.
    }else{
        temp.fadeIn();
    }

    // 화면의 중앙에 레이어를 띄운다.
    if (temp.outerHeight() < $(document).height() ) {
        temp.css('margin-top', '-'+temp.outerHeight()/2+'px');
    }else {
        temp.css('top', '0px');
    }
    if (temp.outerWidth() < $(document).width() ) {
        temp.css('margin-left', '-'+temp.outerWidth()/2+'px');
    }else {
        temp.css('left', '0px');
    }

    $('#dialogBtnClose').click(function() {
        if(bg) {
            $('.layer').fadeOut();
        }else {
            temp.fadeOut();
        }
//            e.preventDefault();
        if(isChrome) {
            location.reload();
        }
        
    });
}

$(document).ready(function() {

    //$(window.document).on("dragstart"  , function(event){return false;});	//드래그
    //$(window.document).on("selectstart", function(event){return false;});	//더블클릭을 통한 선택
    //$(window.document).on("contextmenu", function(event){return false;});	// 우클릭 방지

    if(is_iPad){
        var iPadArr = [
                'js/plugins/handleOpenURL.js'
            ,   'js/plugins/LaunchMyApp.js'
            ,   'js/iap.js'
            ,   'js/purchase.js'
        ];
        var javascript;

        for(var i = 0; i< iPadArr.length; i++){
            javascript = $('<script>',
                {
                    type : 'text/javascript',
                    src  : iPadArr[i]
                });
            $('head').append(javascript);
        }
    }


    new CBPFWTabs(document.getElementById('tabs'));

    //////////////////////////////////////////////////////////////////////////////////////////////////
    document.addEventListener("deviceready", initLogger, false);
    makeArray();
    var paramMap = new Map();
//        $('#btnBack').hide();

    $('body').show();

    //
    // 뷰어 패턴의 좌표 구할 때 사용
    //
    $("#dialogImage").click(function(event) {
        var $x=event.offsetX;
        var $y=event.offsetY;
//        console.log($x +" / " +$y);
    });





    //
    //
    ////
    //// 인덱스 페이지의 도움말 버튼
    ////
    //$("#btnHelp").click(function() {
    //    $('#dialogBtnNext').attr('src', 'img/help/btn_next.png');
    //    $('#dialogImage').attr('src', 'img/help/001.png');
    //    layer_open('layer2');
    //    count = 1;
    //    helpFlag = true;
    //    currentHelpPage = [];
    //    $("#dialogBtnNext").hide();
    //    $("#dialogBtnTopback").hide();
    //});
    //
    ////
    //// 도움말 페이지의 첫 화면에서 확인할 도움말 선택
    ////
    //$(".helpArea").click(function() {
    //    var id = this.id;
    //    if(helpFlag == true) {
    //        for(var i = 0; i < helpPages.length; i++) {
    //            if(helpPages[i].id == id) {
    //                $("#dialogImage").attr("src", helpImgsPath + helpPages[i].images[0]);
    //                currentHelpPage = helpPages[i].images;
    //            }
    //        }
    //        $("#dialogBtnNext").show();
    //        helpFlag = false;
    //    }
    //});
    //
    ////
    //// 도움말 페이지의 다음으로
    ////
    //$("#dialogBtnNext").click(function() {
    //    $("#dialogImage").attr("src", helpImgsPath + currentHelpPage[count]);
    //    if(count == currentHelpPage.length-1) {
    //        $("#dialogBtnNext").hide();
    //        $("#dialogBtnTopback").show();
    //        count = 1;
    //    }else {
    //        count++;
    //    }
    //});
    //
    ////
    //// 도움말 페이지의 첫 화면으로 돌아가기
    ////
    //$("#dialogBtnTopback").click(function() {
    //    $("#dialogImage").attr("src", helpImgsPath + "001.png");
    //    $("#dialogBtnTopback").hide();
    //    helpFlag = true;
    //});

    //
    // 헬프 페이지 버튼
    //
    $('#btnHelp').show();
    $('#btnExit').show();
    $('#certConfigBtn').hide();

    initTabContents();
    setLayout();


    function setLayout() {
//        console.log(location.search);
        var url = location.search.substring(1);
//        console.log(url);
        var arguments = url.split("&");
        if(arguments != null && arguments != '') {
            for(var i in arguments) {
                var keyNValue = arguments.pop().split("=");
                paramMap.put(keyNValue[0], keyNValue[1]);
//                console.log("Param key="+keyNValue[0] + ", value="+keyNValue[1]);
            }
            var tabValue = paramMap.get("tab");
            $(".content-current").each(function() {
                $(this).removeClass('content-current');
            });

            if(tabValue == null || tabValue=='undefined' || tabValue =='') {
                tabValue = "section-history";
            }
            $(".tabButtons").each(function() {
                if(this.href.indexOf('#'+tabValue) > 0) {
                    $('.content section').css('border', '9px #' + this.id + ' solid');
                    $('#'+tabValue).addClass('content-current');
                }
            });

        }
    }

    //
    // 도움말 페이지 닫기
    //
    $("#btnClose").click(function() {
//        window.external.ListenWeb("4", "null", "null");
        window.open('', '_self', '');
        window.close();
    });

    //
    // 인증 버튼
    //
    $("#certConfigBtn").click(function() {
        $("#overlayCert").show();
        $("#modal").show();
        $("#middleBtnOk").focus();
        $("#tabs").hide();
//        $("#tabs").show();
    });

    //
    // 인증 페이지
    // 인증 서버에 입력한 인증 코드 값을 GET 방식으로 전송하고
    // 결과 값에 따라 인증을 체크한다.
    //
    $("#middleBtnOk").click(function() {
        var code = $("#identificationNo").val();

        if(code.length === 0) {
            console.log("License input field is empty");
            $("#resultCert").text("有効な認証コードを入れてください。");
            return ;
        }
        //
        // 라이선스 인증 요청
        //
        $.ajax({
            type: "GET"
            , dataType: "text"
            , contentType: "text/xml"
            , url: Enum.Const.LICENSE_CHECK_URL
            , data: "appId="+Enum.Const.APP_ID +"&code="+code
            , statusCode: {
                200: function (data) {
                    //
                    // 수신된 데이터를 json object 타입으로 변경
                    //
                    // 성공: {"enabledContents":"ALL","code":"ETCWEX8YUBM0600","appId":"ETC01SHA64IPAD","deviceId":null}
                    // 실패: {"errorCode":100,"errorString":"code not found"}
                    //
                    var result = JSON.parse(data);
                    if (result.enabledContents === "ALL") {
                        //
                        // 인증에 성공한 경우 라이선스 체크(.ETC01SHA64IPAD_license.cfg) 파일을 생성한다.
                        //
                        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
                            fileSystem.root.getFile(Enum.Const.LICENSE_FLAG, {create: true, exclusive: false}, function() {
                                console.log("success");
                                $("#overlayCert").hide();
                                $("#modal").hide();
                                $(".certBtn").hide();
                                window.localStorage.setItem('cert', 'true');
                                contentsArray.enabledContents = fullContentsArray.enabledContents;
                                $("#tabs").show();
                                makeArray();
                                initTabContents();
                            });
                        });
                    }
                    if (result.errorCode) {
                        clearResultCert();
                        //$("#resultCert").text("code not found");
                        $("#resultCert").text("認証失敗");
                    }
                }, 401: function() {
                    console.log("code [401] // ERROR ");
                }
                , 500: function() {
                    console.log("code [500] // ERROR");
                    clearResultCert();
                    //$("#resultCert").text("server error");
                    $("#resultCert").text("認証失敗");
                }
            }, error: function (request, status, error) {
                console.log("error: " + error + " code=" + request.status);
                clearResultCert();
                //$("#resultCert").text("connection failed");
                $("#resultCert").text("認証失敗");
            }
        });
        $("#overlayCert").css("height", "2001px");
        $("#modal").css("height", "40%");
    });

    $("#middleBtnCancel").click(function() {
        $("#tabs").show();
        $("#overlayCert").hide();
        $("#modal").hide();
    });

    //
    // 인증 페이지의 구매 버튼의 클릭 이벤트
    //
    $("#purchaseBtn").click(function() {
        clearResultCert();
        // $("#resultCert").append("お客様は既に教材を購入済みです。");
        //
        // 2014. 11. 12 [oskwon-begin]
        //
        app.initialize();
    });

    $("#reinstallBtn").click(function() {
        app.applicationRestore();
    });



}); // end of document ready



function clearResultCert() {
    $("#resultCert").text(" ");
}

function handleOpenURL(url) {
    setTimeout(function() {
        openURL(url);
    },1000);
}

function resize() {

    // 창의 높이를 기준으로 이미지 들의 상대적인 높이를 구한다.

    var height = $(window).height();

    var hdrHeight       = height * 0.1;         // header height
    var strHdrHeight    = hdrHeight + "px";

    var hlpHeight       = hdrHeight * 0.6;
    var hlpTop          = hdrHeight - hlpHeight + (hlpHeight * 0.15); // (hlpHeight * 0.15) = 그림자 영역의 높이

    var tabTopMargin    = 58;
    var tabBottomMargin = 70;

    var contHeight      = height - (hdrHeight + tabTopMargin + tabBottomMargin);



    // header
    $("#layerHeaderButtons").css("height", strHdrHeight);
    $("#layerHeaderButtons").css("background-size", "auto " + strHdrHeight);   // header background


    // help button
    $("#helpBox").css("padding-top" , hlpTop    + "px");
    $("#btnHelp").attr("height"     , hlpHeight + "px");

    $("#content11").css("height"   , contHeight + "px");

    //$("#btnHelp").attr("height", "60px");
    //$("#btnHelp").attr("width", "auto");

    $("#output").attr("value", contHeight * 1000 / 1000 + "px");
}







