
var contentConstant = {
    "CONTENT_PATH": '04-MT000101-1005019/',
    "BACKGROUND_IMAGE" : 'char_background.png',
    "Margin": 20,
    "ScaleWidth" : 0,
    "ScaleHeight" : 0,
    "LeftPaddingScale" : 0,
    "TopPaddingScale":  0,
    "ImageWidth": 740,
    "ImageHeight": 500
};

var CONTENTS_INFO = [
     {btn: 'BTN_BOTTOM_MIDDLE_FIRST',       imgId: ["01_01","01_02","01_03","EMPTY"]}
    ,{btn: "none",                          imgId: '01_01'}
    ,{btn: "none",                          imgId: '01_02'}
    ,{btn: "none",                          imgId: '01_03'}
    ,{btn: "BTN_BOTTOM_MIDDLE_SECOND",      imgId: 'titleOn'}

    ,{btn: 'none',                          imgId: 'img'}
    ,{btn: 'BTN_BOTTOM_ALL',                imgId: 'img3'}
];

var CONTENTS_INFO_2 = [];

var GRAPH_INFO = [
    'img','img3',
    "01_01","01_02","01_03","titleOn"
];

var scaleFactor;
var flagZoom = false;

var imageTop;
var imageLeft;
var position;
var baseImg = "#img1";

var title;
var pageURL;
var contentID;

//var oCONTENTS_INFO_2 = JSON.parse(JSON.stringify(CONTENTS_INFO_2));

$(document).ready(function() {
    initBatchViewer.call(this);

});
