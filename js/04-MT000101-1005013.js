
var contentConstant = {
    "CONTENT_PATH": '04-MT000101-1005001/',
    "BACKGROUND_IMAGE" : 'char_background.png',
    "Margin": 20,
    "ScaleWidth" : 0,
    "ScaleHeight" : 0,
    "LeftPaddingScale" : 0,
    "TopPaddingScale":  0,
    "ImageWidth": 740,
    "ImageHeight": 500
};

var CONTENTS_INFO = [
     {btn: 'BTN_BOTTOM_MIDDLE_FIRST',   imgId: ['01_01', '01_02', '01_03', '01_04', '01_05']}

    ,{btn: 'none',                      imgId: '01_02'}
    ,{btn: 'none',                      imgId: '01_03'}
    ,{btn: 'none',                      imgId: '01_04'}
    ,{btn: 'none',                      imgId: '01_05'}
    ,{btn: 'none',                      imgId: 'img'}
    ,{btn: 'BTN_BOTTOM_ALL',            imgId: 'img3'}
    ,{btn: 'BTN_BOTTOM_MIDDLE_SECOND',            imgId: 'titleOn'}
];


var GRAPH_INFO = [
    'img','img3',
    "01_00","02_00","03_00","04_00","05_00",
    "01_01","01_02","01_03","01_04","01_05","01_06","01_07","01_08","01_09","01_10",
    "02_01","02_02","02_03","02_04","02_05","02_06","02_07","02_08","02_09","02_10",
    "03_01","03_02","03_03","03_04","03_05","03_06","03_07","03_08","03_09","03_10",
    "04_01","04_02","04_03","04_04","04_05","04_06","04_07","04_08","04_09","04_10"
];

var CONTENTS_INFO_2 = [];

var scaleFactor;
var flagZoom = false;

var imageTop;
var imageLeft;
var position;
var baseImg = "#img1";

var title;
var pageURL;
var contentID;


$(document).ready(function() {
    initBatchViewer.call(this);
});
