function Enum() {}
Enum.Const = {
    LOG_FILE_NAME: '.html5app.log'
    , LOG_CONFIG_FILE_NAME: '.ETC01SHA64IPAD_log.cfg'
    , CRLF: '\r\n'
    , DEFAULT_SCHOOL_CODE: "VB_DEV"
    , DEFAULT_CONTENT_CODE: "VB_DEV"
    , DEFAULT_PROCESS_NAME: "HTML5"
    , LICENSE_FLAG: ".ETC01SHA64IPAD_license.cfg"
    , APP_ID: "ETC01SHA64IPAD"
    , LICENSE_CHECK_URL: "http://et.chieru.co.jp/flashcontents/check_code2.php"
}

Enum.LogFlag = {
    START: 'start'
    , STOP: 'stop'
};
Enum.AppenderType = {
    CONSOLE: 'CONSOLE'
    , FILE:'FILE'
    , HTTP_POST: 'HTTP_POST'
};
Enum.Level = {
    DEBUG: "DEBUG"
    , INFO: "INFO"
    , WARN: "WARN"
    , ERROR: "ERROR"
};