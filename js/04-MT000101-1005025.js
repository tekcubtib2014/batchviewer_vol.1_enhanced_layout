
var contentConstant = {
    "CONTENT_PATH": '04-MT000101-1005025/',
    "BACKGROUND_IMAGE" : 'char_background.png',
    "Margin": 20,
    "ScaleWidth" : 0,
    "ScaleHeight" : 0,
    "LeftPaddingScale" : 0,
    "TopPaddingScale":  0,
    "ImageWidth": 740,
    "ImageHeight": 500
};

var CONTENTS_INFO = [
     {btn: 'BTN_BOTTOM_MIDDLE_FIRST',       imgId: '01_00'}
    ,{btn: 'BTN_BOTTOM_MIDDLE_SECOND',      imgId: '02_00'}

    ,{btn: 'none',                          imgId: 'img'}
    ,{btn: 'BTN_BOTTOM_ALL',                imgId: 'img3'}
];

var CONTENTS_INFO_2 = [
     {btn: 'BTN_BOTTOM_MIDDLE_FIRST',   images:["01_01","01_02","01_03","01_04","01_05","01_06","01_07","01_08","01_09","01_10","01_11","01_12","EMPTY"]}
    ,{btn: 'BTN_BOTTOM_MIDDLE_SECOND',  images:["02_01","02_02","02_03","02_04","02_05","02_06","02_07","02_08","02_09","02_10","02_11","02_12","EMPTY"]}
];

var GRAPH_INFO = [
    'img','img3',
    "01_00","02_00",
    "01_01","01_02","01_03","01_04","01_05","01_06","01_07","01_08","01_09","01_10","01_11","01_12",
    "02_01","02_02","02_03","02_04","02_05","02_06","02_07","02_08","02_09","02_10","02_11","02_12"
];

var scaleFactor;
var flagZoom = false;

var imageTop;
var imageLeft;
var position;
var baseImg = "#img1";

var title;
var pageURL;
var contentID;

var oCONTENTS_INFO_2 = JSON.parse(JSON.stringify(CONTENTS_INFO_2));

$(document).ready(function() {
    initBatchViewer.call(this);

});
