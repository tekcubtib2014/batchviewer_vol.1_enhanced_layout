
var contentConstant = {
    "CONTENT_PATH": '04-MT000101-1005022/',
    "BACKGROUND_IMAGE" : 'char_background.png',
    "Margin": 20,
    "ScaleWidth" : 0,
    "ScaleHeight" : 0,
    "LeftPaddingScale" : 0,
    "TopPaddingScale":  0,
    "ImageWidth": 740,
    "ImageHeight": 500
};


CONTENTS_INFO = [
    {btn: 'none',                 imgId: 'img'}
    ,{btn: 'BTN_BOTTOM_ALL',      imgId: 'img3'}

    ,{btn: 'none',                imgId: '01_01'}
    ,{btn: 'none',                imgId: '01_02'}
    ,{btn: 'none',                imgId: '01_03'}
    ,{btn: 'none',                imgId: '01_04'}
    ,{btn: 'none',                imgId: '02_01'}
    ,{btn: 'none',                imgId: '02_02'}
    ,{btn: 'none',                imgId: '02_03'}
    ,{btn: 'none',                imgId: '02_04'}
    ,{btn: 'none',                imgId: '03_01'}
    ,{btn: 'none',                imgId: '03_02'}
    ,{btn: 'none',                imgId: '03_03'}
    ,{btn: 'none',                imgId: '03_04'}
    ,{btn: 'none',                imgId: '04_01'}
    ,{btn: 'none',                imgId: '04_02'}
    ,{btn: 'none',                imgId: '04_03'}
    ,{btn: 'none',                imgId: '04_04'}
];


var CONTENTS_INFO_ = [
    {
        btn:       'btn_Oil',
        imgId:     ["04_01","04_02","04_03"],
        nations:   ["btn_Saudi", "btn_UAE", "btn_Katar"]
    }
    ,{
        btn:        'btn_Electric',
        imgId:      ["01_01","01_02","01_03"],
        nations :   ["btn_China", "btn_America", "btn_Taiwan"]
    }
    ,{
        btn:        'btn_Gas',
        imgId:      ["03_01","03_02","03_03"],
        nations :   ["btn_Katar", "btn_Malaysia", "btn_Australia"]
    }
    ,{
        btn:        'btn_Food',
        imgId:      ["02_01","02_02","02_03"],
        nations :   ["btn_America", "btn_China", "btn_Australia"]
    }
];

var CONTENTS_INFO_2 = [];

var GRAPH_INFO = [
    'img','img3',
    "01_01","01_02","01_03","01_04",
    "02_01","02_02","02_03","02_04",
    "03_01","03_02","03_03","03_04",
    "04_01","04_02","04_03","04_04"
];

var scaleFactor;
var flagZoom = false;

var imageTop;
var imageLeft;
var position;
var baseImg = "#img1";

var title;
var pageURL;
var contentID;

var item;
var nation;



$(document).ready(function() {
    initBatchViewer.call(this);

    /**
     * Created by Administrator on 2015-04-15.
     */
    function initBatchViewer() {

        /**
         * 타이틀 설정
         *
         */
        setTitle.call(this);

        load(baseImg, flagZoom);

        $(window).resize(function () {
            load(baseImg, flagZoom);
            $(".bottomBtn").css('width', $(window).width() * 0.06);          // 보기 버튼들
            $("#BTN_BOTTOM_MODE").css('width', $(window).width() * 0.12);    // 모드 토글 버튼
            $(".titleOn").css('width', $(window).width() * 0.09);
        });

        $(".graphImage").hide();
        $("#img").show();
        $("#img1").show();
        $("#img3").hide();

        $("#mainTable").on("dragstart"  , function(event){return false;});	// 드래그 방지
        $(window.document).on("selectstart", function(event){return false;});	// 더블클릭을 통한 선택 방지
        $(window.document).on("contextmenu", function(event){return false;});	// 우클릭 방지

        /**
         * 다이얼로그 윈도우의 버튼들에 대한 동작
         *
         * 버튼이 클리되면 해당 버튼의 이미지를 on / off 토글한다.
         * 해당 버튼과 매핑된 이미지를 표시한다.
         *
         * 전체보기     : 모든 그래프 이미지를 표시한다.
         * 모드 토글
         * 번들뷰
         * 유닛뷰
         *
         */


        /**
         * 전체보기 버튼 동작.
         *
         * 전체보기가 클릭되면 다른 모든 버튼은 초기화 된다.
         * 모드토글, 번들뷰, 유닛 뷰 역시 전체보기 버튼을 초기화한다.
         *
         */
        $("#BTN_BOTTOM_ALL").click(function () {
            toggleEntireView.call(this);
        });

        /**
         * 버튼 사이즈 설정
         *
         */
        $(".bottomBtn").css('width', '100px');          // 보기 버튼들
        $("#BTN_BOTTOM_MODE").css('width', '200px');    // 모드 토글 버튼
        $(".titleOn").css("width", '150px');

        /**
         * [2015.04.20]
         * 팝업 버튼 jquery draggable() 로 처리.
         *
         */
        $('#btnCloseDialog').click(function () {
            $('#dialog').hide();
            $('#showDialog')[0].src = getToggleImageName($('#showDialog')[0].src);
        });

        /**
         * 파렛트 표시
         *
         */
        $('#showDialog').click(function () {

            if ($("#BTN_BOTTOM_ALL")[0].src.indexOf("toggle") > 0) {
                $('#img3').hide();
                toggleONTargetButton($("#BTN_BOTTOM_ALL"));
            }

            if (this.src.indexOf('toggle') > 0) {
                $('.dialog').hide();
            } else if (this.src.indexOf('toggle') == -1) {
                $('.dialog').show();
            }
            $(this)[0].src = getToggleImageName($(this)[0].src);
        });

        /**
         * 타이틀 토글
         *
         */
        $("#btnToggleTitle").click(function() {
            toggleTitle.call(this);
        });

        $("#btnLockScreen").click(function() {
            this.src = getToggleImageName(this.src);
            if (this.src.indexOf('toggle') > 0) {
                $("#mask").hide();
            } else if (this.src.indexOf('toggle') == -1) {
                $("#mask").show();
            }
        });
    }

    var clickCount = 0;
    var currentURL = $(location).attr('pathname');
    currentURL = currentURL.substring(currentURL.lastIndexOf("/"));

    /**
     * [jgkim] 2015.04.15
     * 이미지 조정 시작
     * 아이패드에서 딜레이 필요(확인 중)
     * @param baseImg
     * @param flagZoom
     */
    function load(baseImg, flagZoom) {

        sizeToFit("#container", baseImg, true);

        if (flagZoom == true) {
            sizeToFit("#container", baseImg, false);
        }

        sizeToFit2(baseImg);
    }

    /**
     * [jgkim] 2015.04.15
     * 이미지 리사이즈 주 로직.
     * @param source
     * @param dest
     * @param isSet
     */
    function sizeToFit(source, dest, isSet) {

        var marginH = 50;   // margin height

        var tWidth = $(source).width();             // target width
        var tHeight = $(window).height();           // target height

        var bottomHeight = $("#bottom").height();
        tHeight -= bottomHeight + marginH;

        var sWidth = $(dest)[0].naturalWidth;
        var sHeight = $(dest)[0].naturalHeight;

        var rateWidth = tWidth / sWidth;
        var rateHeight = tHeight / sHeight;

        var rate = rateWidth < rateHeight ? rateWidth : rateHeight;
        var width = sWidth * rate;
        var height = sHeight * rate;

        $(dest).attr("width", width + "px");
        $(dest).attr("height", height + "px");

        if (isSet == true)
            scaleFactor = rate;
    }

    /**
     * [jgkim] 2015.04.15
     * 이미지 오버레이에 사용하기 위해 포지션을 절대 좌표로 설정.
     * @param baseImg
     */
    function sizeToFit2(baseImg) {
        position = $(baseImg).offset();
        for (var i = 0; i < CONTENTS_INFO.length; i++) {
            var img = "#" + CONTENTS_INFO[i].imgId;
            adjustGraphImages(img, baseImg);
        }
        $(baseImg).show();
    }

    function adjustGraphImages(img, baseImg) {
        sizeToFit("#container", img, false);
        imageLeft = ( ($('#container').width() - $(baseImg).width()) / 2 ) + 150;
        imageTop = ($(window).height() - $(baseImg).height()) / 2 + 20;
        //imageTop = 45;

        $(img).css({
            position: "absolute",
            left: imageLeft,
            top: imageTop
        });

        $('#dialog').css({
            left: "150px",
            top: "100px"
        });

        $("#mask").css({
            width: $(window).width() - 150,
            height: $(window).height(),
            position: "absolute",
            top: 0,
            left: "150px",
            opacity: 0,
            backgroundColor: "black",
            'z-index': 998,
            display: 'none'
        });
    }

    /**
     * [jgkim] 2015.04.17
     * 타이틀 설정
     *
     *
     */
    function setTitle() {
        pageURL = this.location.href.substring(this.location.href.lastIndexOf('/') + 1);
        contentID = pageURL.substring(0, pageURL.lastIndexOf('.'));

        for (var i = 0; i < contentsArray.enabledContents.length; i++) {
            if (contentID === contentsArray.enabledContents[i].cd) {
                title = contentsArray.enabledContents[i].tit;
            }
        }
        $("title").html(title);
        $("#titleArea").html(title);

        ///
        /// [jgkim] 2015.04.20
        /// 팝업 버튼
        ///
        $('#dialog').show();
        $('#dialog').draggable({
            containment: "#mainTable",
            iframeFix: true,
            zIndex: 998
        });

        $('#dialog').css({
            left: "100px",
            top: "150px",
            "background-color": "#FDEADA",
            "opacity" : 1,
            "border" : "2px solid #FF6699",
            "z-index" : "998",
            "position" : "absolute",
            "display" : "none",
            "border-radius" : "10px"
        });

        $('#showDialog').css({
            "width" : "126px",
            "max-width" : "100%",
            "display" : "block",
            "margin-left" : "-12px",
            "margin-top" : "50px",
            "position" : "relative"
        });

        $('#dialog').hide();
    }

    /**
     * [jgkim] 2015.04.21
     * 타이틀 토글
     *
     */
    function toggleTitle() {

        this.src = getToggleImageName(this.src);
        var titleTag = $('#titleArea');
        if(titleTag.text().length > 1) {
            titleTag.text('');
        } else {
            titleTag.text(title);
        }
    }

    /**
     * [jgkim] 2015.04.17
     * 전체보기(엔타이어뷰) 동작
     *
     */
    function toggleEntireView() {
        if (this.src.indexOf('toggle') > 0) {
            $('#img3').hide();
        } else {
            initializeGraphImgs();
            $('#img3').show();
        }
        $(this)[0].src = getToggleImageName($(this)[0].src);
        clickCount = 0;
    }




    /**
     * [jgkim] 2015.06.15
     * 품목 버튼 클릭 시 해당 국가 버튼 만 활성화
     *
     */
    $(".btnBundleView").click(function() {

        if ($("#BTN_BOTTOM_ALL")[0].src.indexOf("toggle") > 0) {
            $('#img3').hide();
            toggleONTargetButton($("#BTN_BOTTOM_ALL"));
        }
        
        $(".btnUnitView").each(function() {
            toggleOFFTargetButton(this);
        });

        for (var i = 0; i < CONTENTS_INFO_.length; i++) {
            item = this.id;
            if (this.id == CONTENTS_INFO_[i].btn) {
                for (var j = 0; j < CONTENTS_INFO_[i].nations.length; j++) {
                    nation = CONTENTS_INFO_[i].nations[j];
                    toggleONTargetButton($("#" + nation));
                }
            }
        }
    });

    /**
     * [jgkim] 2015.06.15
     * 국가 버튼 클릭 시
     *
     */
    $(".btnUnitView").click(function() {
        if (this.src.indexOf('toggle') > 0) {
            return;
        }
        for (var i = 0; i < CONTENTS_INFO_.length; i++) {
            if (item == CONTENTS_INFO_[i].btn) {
                for (var j = 0; j < CONTENTS_INFO_[i].nations.length; j++) {
                    if (this.id == CONTENTS_INFO_[i].nations[j]) {
                        var graphImg = $("#" + CONTENTS_INFO_[i].imgId[j]);
                        $(graphImg).show();
                    }
                }
            }
        }
    });

    function toggleOFFTargetButton(target) {
        $(target)[0].src = getToggleOffImageName($(target)[0].src);
    }

    function toggleONTargetButton(target) {
        $(target)[0].src = getToggleOnImageName($(target)[0].src);
    }

    function initializeGraphImgs() {
        $('.unitView').hide();
        $('.bundleView').hide();
        $('.btnUnitView').each(function() {
            $(this)[0].src = getToggleOnImageName($(this)[0].src);
        });
        if($('#BTN_BOTTOM_MODE').length > 0) {
            CONTENTS_INFO_2 = JSON.parse(JSON.stringify(oCONTENTS_INFO_2));
        }
        clickCount = 0;
    }
});


