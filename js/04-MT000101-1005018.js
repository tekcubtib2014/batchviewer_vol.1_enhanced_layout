
var contentConstant = {
    "CONTENT_PATH": '04-MT000101-1005005/',
    "BACKGROUND_IMAGE" : 'char_background.png',
    "Margin": 20,
    "ScaleWidth" : 0,
    "ScaleHeight" : 0,
    "LeftPaddingScale" : 0,
    "TopPaddingScale":  0,
    "ImageWidth": 740,
    "ImageHeight": 500
};

var CONTENTS_INFO = [
     {btn: 'BTN_BOTTOM_MIDDLE_FIRST',       imgId: ["02_01","02_02","02_03","02_04","02_05","02_06","EMPTY"]}
    ,{btn: "none",                          imgId: '02_01'}
    ,{btn: "none",                          imgId: '02_02'}
    ,{btn: "none",                          imgId: '02_03'}
    ,{btn: "none",                          imgId: '02_04'}
    ,{btn: "none",                          imgId: '02_05'}
    ,{btn: "none",                          imgId: '02_06'}
    ,{btn: "BTN_BOTTOM_MIDDLE_SECOND",      imgId: 'titleOn'}

    ,{btn: 'none',                          imgId: 'img'}
    ,{btn: 'BTN_BOTTOM_ALL',                imgId: 'img3'}
];

var CONTENTS_INFO_2 = [];

var GRAPH_INFO = [
    'img','img3',
    "02_01","02_02","02_03","02_04","02_05","02_06"
];

var scaleFactor;
var flagZoom = false;

var imageTop;
var imageLeft;
var position;
var baseImg = "#img1";

var title;
var pageURL;
var contentID;

//var oCONTENTS_INFO_2 = JSON.parse(JSON.stringify(CONTENTS_INFO_2));

$(document).ready(function() {
    initBatchViewer.call(this);

});
