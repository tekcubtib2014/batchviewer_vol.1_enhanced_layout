var IS_DEBUG = false
var IAP = {
    list: [ 'ETC01SHA64IPAD' ],
    products: {}
};
var localStorage = window.localStorage || {};

IAP.initialize = function () {
    if (!window.storekit) {
        $("#resultCert").text('In-App Purchases not available');
        return;
    }
    $("#resultCert").text("購入中...");

    // Initialize
    storekit.init({
        debug:    true,
        noAutoFinish: true,
        ready:    IAP.onReady,
        purchase: IAP.onPurchase,
        finish:   IAP.onFinish,
        restore:  IAP.onRestore,
        error:    IAP.onError,
        restoreCompleted: IAP.onRestoreCompleted
    });
};

IAP.onReady = function () {
    // Once setup is done, load all product data.
    storekit.load(IAP.list, function (products, invalidIds) {
        for (var j = 0; j < products.length; ++j) {
            var p = products[j];
            IAP.products[p.id] = p;
        }
        if(products.length > 0) {
            var p = products[0];
            $("#resultCert").text("Application id is " + p.id);
            IAP.loaded = true;
            IAP.render(p);
        }
    });
};

IAP.onPurchase = function (transactionId, productId) {
    var n = (localStorage['storekit.' + productId]|0) + 1;
    localStorage['storekit.' + productId] = n;
    if (IAP.purchaseCallback) {
        IAP.purchaseCallback(productId);
        delete IAP.purchaseCallbackl;
    }

    storekit.finish(transactionId);

    storekit.loadReceipts(function (receipts) {
        //
        // 컨텐츠 구매 성공
        //
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
            fileSystem.root.getFile(Enum.Const.LICENSE_FLAG, {create: true, exclusive: false}, function() {
                $("#overlayCert").hide();
                $("#modal").hide();
                $(".certBtn").hide();
                contentsArray.enabledContents = fullContentsArray.enabledContents;
                $("#tabs").show();
                makeArray();
                initTabContents();

            });
        });
        $("#resultCert").text("DONE!!");
    });
};

IAP.onFinish = function (transactionId, productId) {
    $("#resultCert").text("");
};

IAP.onError = function (errorCode, errorMessage) {
    alert('Error: ' + errorMessage);
};

IAP.onRestore = function (transactionId, productId) {
    $("#resultCert").text("払い戻し: " + productId);
    var n = (localStorage['storekit.' + productId]|0) + 1;
    localStorage['storekit.' + productId] = n;
};

IAP.onRestoreCompleted = function () {
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
        fileSystem.root.getFile(Enum.Const.LICENSE_FLAG, {create: true, exclusive: false}, function() {
            $("#overlayCert").hide();
            $("#modal").hide();
            $(".certBtn").hide();
            contentsArray.enabledContents = fullContentsArray.enabledContents;
            $("#tabs").show();
            makeArray();
            initTabContents();

        });
    });
    $("#resultCert").text("DONE!!");
};

IAP.buy = function (productId, callback) {
    IAP.purchaseCallback = callback;
    storekit.purchase(productId);
};

IAP.restore = function () {
    storekit.restore();
};

IAP.fullVersion = function () {
    return localStorage['storekit.ETC01SHA64IPAD'];
};
