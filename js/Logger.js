var Logger = (function() {
    function Logger(args) {

        var args = args || {};
        var os;
        var version;


        var logConfiguration = {};


        var mainAppender   = args.mainAppender;
        var backupAppender = args.backupAppender;
        var debugAppender  = new ConsoleAppender("Debug");

        var schoolId     = (args.schoolId === undefined ? Enum.Const.DEFAULT_SCHOOL_CODE : args.schoolId);
        var contentId    = (args.contentId === undefined ? Enum.Const.DEFAULT_CONTENT_CODE : args.contentId);
        var processName    = (args.processName === undefined ? Enum.Const.DEFAULT_PROCESS_NAME : args.processName);

        var eventLog = [];



        /////////////////////////////////////////////////
        //              Public                         //
        /////////////////////////////////////////////////
        this.getMainAppender = function() {
            return mainAppender;
        }

        this.getBackupAppender = function() {
            return backupAppender;
        }

        this.getDebugAppender = function() {
            return debugAppender;
        }

        var setOS = function(osName) {
            os = osName;
        }

        var setVersion = function(osVersion) {
            version = osVersion;
        }


        this.getOS = function() {
            return os;
        }

        this.getVersion = function() {
            return version;
        }

        this.getSchooId = function() {
            return schoolId;
        }

        this.getContentId = function() {
            return contentId;
        }
        this.getProcessName = function() {
            return processName;
        }

        this.getEventLog = function() {
            return eventLog;
        }

        this.getLogConfiguration = function() {
            return logConfiguration;
        }

        this.parseMsg = function (flag, schoolId, contentId) {
            var logFomattedTime = getLogFormattedDate();
            var currTime = getYYYYMMDDHHmmss();
            return {
                logDate       : logFomattedTime
                , eventType     : (flag === Enum.LogFlag.START ? "0" : "1")
                , eventTime     : currTime
                , schoolId      : schoolId
                , contentId     : contentId
            }
        }

        this.getLogHeader = function() {
            var headerLog = {
                unitId        : ""
                , hostName      : ""
                , os            : this.getOS() + " " +this.getVersion()
                , architecture  : "0"
                , processName   : this.getProcessName()
                , logType       : "0"
            };
            var data = "";
            for (var key in headerLog) {
                data += "\"".concat(headerLog[key]).concat("\"").concat(",");
            }
            data = data.slice(0, data.length - 1);
            return data;
        }

        this.getLogBody = function() {
            var data = "";
            for ( var index in eventLog ) {
                var logMsg = eventLog[index];
                var bodyLog = {
                    schoolId      : logMsg.schoolId
                    , gradeCode     : ""
                    , classCode     : ""
                    , subjectCode   : ""
                    , courseCode    : ""
                    , contentId     : logMsg.contentId
                    , contentType   : "0"
                    , invoker       : "0"
                    , eventType     : logMsg.eventType
                    , eventTime     : logMsg.eventTime
                    , loginId       : ""
                    , studentName   : ""
                    , studentCode   : ""
                    , result        : ""
                }
                for (var key in bodyLog) {
                    data += "\"".concat(bodyLog[key]).concat("\"").concat(",");
                }
                data = data.slice(0, data.length - 1);
                data += Enum.Const.CRLF;
            }
            return data;
        }

        //
        // Setup the device information and logconfiguration
        //
        function initConfig() {
            setOS(device.platform);
            setVersion(device.version);

            //
            // Get log configuration from file 'html5app.cfg'
            //

            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
                fileSystem.root.getFile(Enum.Const.LOG_CONFIG_FILE_NAME, {create: true, exclusive: false}, function(fileEntry) {
                    fileEntry.file(function(file) {
                        var fileReader = new FileReader();
                        fileReader.onloadend = function(evt) {
                            var configuration = evt.target.result;
                            console.log(configuration);
                            if(configuration.length == 0) {
                                logConfiguration = {
                                    enabled: false
                                    , url : ''
                                }
                                fileEntry.createWriter(function(writer) {
                                    writer.write(JSON.stringify(logConfiguration));
                                    writer.abort();
                                });
                            } else {
                                try {
                                    logConfiguration = JSON.parse(configuration);
                                    console.log(JSON.stringify(logConfiguration));
                                } catch (e) {
                                    //
                                    // Put default configuration when can't get configuration.
                                    //
                                    logConfiguration = {
                                        enabled: false
                                        , url : ''
                                    }
                                    fileEntry.createWriter(function(writer) {
                                        writer.write(JSON.stringify(logConfiguration));
                                        writer.abort();
                                    });
                                }
                            }
                        }
                        fileReader.readAsText(file);
                    })
                })
            })


        }
        document.addEventListener("deviceready", initConfig, false)
    }

    Logger.prototype.add = function(flag) {
        if( !this.getLogConfiguration().enabled ) {
            return ;
        }
        var formattedMsg = this.parseMsg( flag
            , this.getSchooId()
            , this.getContentId());
        if(!(this.getDebugAppender() === undefined)) {
            this.getDebugAppender().log(formattedMsg, undefined);
        }
        this.getEventLog().push(formattedMsg);
    }

    Logger.prototype.add = function(flag, contentId) {
        if( !this.getLogConfiguration().enabled ) {
            return ;
        }
        var formattedMsg = this.parseMsg( flag
            , this.getSchooId()
            , contentId);
        if(!(this.getDebugAppender() === undefined)) {
            this.getDebugAppender().log(formattedMsg, undefined);
        }
        this.getEventLog().push(formattedMsg);
    }

    Logger.prototype.write = function() {
        if( !this.getLogConfiguration().enabled ) {
            return ;
        }
        var logHeader = this.getLogHeader();
        var logBody = this.getLogBody();
        if(logBody.length < 1) {
            return ;
        }
        var data = logHeader + Enum.Const.CRLF + logBody;
        console.log(data);
        console.log(JSON.stringify(this.getLogConfiguration()));
        this.getMainAppender().setTargetUrl(this.getLogConfiguration().url);
        this.getMainAppender().write(data, function(data) {
            this.mainAppender.logBackup(data);
        });
        this.getEventLog().length = 0;
    }




    /**
     * Singleton Instance
     */
    var INSTANCE;
    return {
        getInstance: function(args) {
            if (INSTANCE === undefined) {
                INSTANCE = new Logger(args);
            }
            return INSTANCE;
        }
    };
})();